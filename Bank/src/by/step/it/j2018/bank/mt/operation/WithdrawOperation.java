package by.step.it.j2018.bank.mt.operation;

import by.step.it.j2018.bank.mt.model.Account;

public class WithdrawOperation extends Operation{
	public WithdrawOperation(Account account1, double amount) {
		super(account1, amount);
	}

	@Override
	public OperationResult call() throws Exception {
		OperationResult operationResult = new OperationResult(this);
		operationResult.setSuccess(getAccount1().withdraw(getAmount()));
		operationResult.setComplete(true);
		return operationResult;
	}
}
