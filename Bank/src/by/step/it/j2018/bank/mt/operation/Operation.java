package by.step.it.j2018.bank.mt.operation;

import java.util.concurrent.Callable;

import by.step.it.j2018.bank.mt.model.Account;

public abstract class Operation implements Callable<OperationResult>{
	private static long idCounter;
	private long id;
	private Account account1;
	private Account account2;
	private double amount;
	private long timeEnd;
	
	public Operation(Account account1, double amount) {
		super();
		this.id = idCounter++;
		this.account1 = account1;
		this.amount = amount;
		this.timeEnd =System.currentTimeMillis();
	}

	public Operation(Account account1, Account account2, double amount) {
		super();
		this.id = idCounter++;
		this.account1 = account1;
		this.account2 = account2;
		this.amount = amount;
		this.timeEnd =System.currentTimeMillis();
	}

	public static long getIdCounter() {
		return idCounter;
	}

	public long getId() {
		return id;
	}

	public Account getAccount1() {
		return account1;
	}

	public Account getAccount2() {
		return account2;
	}

	public double getAmount() {
		return amount;
	}

	public long getTimeEnd() {
		return timeEnd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((account1 == null) ? 0 : account1.hashCode());
		result = prime * result + ((account2 == null) ? 0 : account2.hashCode());
		long temp;
		temp = Double.doubleToLongBits(amount);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + (int) (timeEnd ^ (timeEnd >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Operation other = (Operation) obj;
		if (account1 == null) {
			if (other.account1 != null)
				return false;
		} else if (!account1.equals(other.account1))
			return false;
		if (account2 == null) {
			if (other.account2 != null)
				return false;
		} else if (!account2.equals(other.account2))
			return false;
		if (Double.doubleToLongBits(amount) != Double.doubleToLongBits(other.amount))
			return false;
		if (id != other.id)
			return false;
		if (timeEnd != other.timeEnd)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Operation [id=" + id + ", account1=" + account1 + ", account2=" + account2 + ", amount=" + amount
				+ ", timeEnd=" + timeEnd + "]";
	}
	
	

}
